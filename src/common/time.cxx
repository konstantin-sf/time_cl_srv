#include "time.hh"

void timestamp_t::set_now() {
  ts_ = system_clock::now();
}

unsigned long long timestamp_t::get_count() const {
  return duration_cast<milliseconds>( ts_.time_since_epoch() ).count();
}

void timestamp_t::copy_val_to_buf( std::string& buf ) const {
  buf = std::to_string( duration_cast<milliseconds>( ts_.time_since_epoch() ).count() );
}

bool timestamp_t::from_buf( const char* buf ) {
  unsigned long long value;

  try {
    value = std::stoull( buf );
  }
  catch( std::exception ex ) {
    fprintf( stderr, "Recevied unexpexted data, buf dump:\"%s\"", buf );

    return false;
  }

  ts_ = time_point<system_clock>( milliseconds( value ) );

  return true;
}

void timestamp_t::print_diff( const timestamp_t& other ) const {
  auto my_ts  = this->get_count();
  auto his_ts = other.get_count();

  std::cout << "Own      timestamp: " << my_ts  << "\n";
  std::cout << "Received timestamp: " << his_ts << "\n";
  std::cout << "Diff:               " << my_ts - his_ts << std::endl;
}
