#include "network.hh"

#include <stdio.h>

#include <errno.h>
#include <unistd.h>

#include <sys/socket.h>

#include <string.h>

bool socket_t::init() {
  bool rc = false;

  do {
    fd_ = socket( AF_INET, SOCK_DGRAM, 0 );

    if( !ok( fd_, "create socket" ) )
      break;

    if( int yes = true;
        !ok( setsockopt( fd_, SOL_SOCKET, SO_BROADCAST, &yes, sizeof( yes ) ), "set broadcast option" ) ||
        !ok( setsockopt( fd_, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof( yes ) ), "set reuseaddr option" ) ) {
      break;
    }

    rc = true;
  } while( false );

  return rc;
}

bool socket_t::bind( const sockaddr_in& addr ) {
  return ok( ::bind( fd_, (const sockaddr*)&addr, sizeof( addr ) ), "bind socket" );
}

bool socket_t::send_to( const sockaddr_in& addr, const char* buf, size_t buflen  ) {
  return ok( sendto( fd_, buf, ++buflen, 0, (const sockaddr*)&addr, sizeof( addr) ), "send data" );
}

bool socket_t::read( char* buf, size_t buflen ) {
  int rc = recvfrom( fd_, buf, buflen, 0, nullptr, 0 );

  if( !ok( rc, "read data" ) )
    return false;

  if( 0 == rc )
    fprintf( stderr, "EOF reached" );

  return true;
}

void socket_t::close_socket() {
  if( -1 != fd_ )
    close( fd_ );
}

bool socket_t::ok( int rc, const char* oper ) {
  if( -1 == rc ) {
    close_socket();

    if( oper != nullptr )
      fprintf( stderr, "Unable to %s, because of %s\n", oper, strerror( errno ) );
  }

  return -1 != fd_;
}
