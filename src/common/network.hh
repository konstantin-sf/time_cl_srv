#pragma once

#include <arpa/inet.h>

#define PORT 61016

class socket_t {
public:
  socket_t() : fd_( -1 ) {};
  ~socket_t() {
    close_socket();
  }

  bool init();
  bool bind( const sockaddr_in& addr );

  bool send_to( const sockaddr_in& addr, const char* buf, size_t buflen  );
  bool read( char* buf, size_t buflen );

private:
  void close_socket();
  bool ok( int rc, const char* oper );

  int fd_;
};
