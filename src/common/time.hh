#pragma once

#include <chrono>
#include <iostream>

using namespace std::chrono;

class timestamp_t {
  public:
    timestamp_t() : ts_( system_clock::now() ) {}

    void set_now();

    bool from_buf( const char* buf );

    unsigned long long get_count() const;

    void copy_val_to_buf( std::string& buf ) const;

    void print_diff( const timestamp_t& other ) const;

  private:
    time_point<system_clock> ts_;
};
