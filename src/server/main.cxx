#include "../common/network.hh"
#include "../common/time.hh"

#include <thread>

int main( int, char** ) {
  socket_t sock;

  if( !sock.init() )
    return 1;

  struct sockaddr_in addr;

  addr.sin_family      = AF_INET;
  addr.sin_port        = htons ( PORT );
  addr.sin_addr.s_addr = INADDR_BROADCAST;

  std::string buf;

  timestamp_t ts;
  ts.copy_val_to_buf( buf );

  while( sock.send_to( addr, buf.data(), buf.size() ) ) {
    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    ts.set_now();
    ts.copy_val_to_buf( buf );
  }

  return 0;
}
