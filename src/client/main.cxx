#include "../common/network.hh"
#include "../common/time.hh"

#include <thread>
#include <sstream>

int main( int, char** ) {
  struct sockaddr_in addr;

  addr.sin_family      = AF_INET;
  addr.sin_port        = htons ( PORT );
  addr.sin_addr.s_addr = INADDR_ANY;

  socket_t sock;

  if( !sock.init() ||
      !sock.bind( addr ) )
    return 1;

  const size_t buflen       = 16;
  char         buf[buflen]  = {};

  timestamp_t ts_current;
  timestamp_t ts_received;

  while( sock.read( buf, buflen ) ) {
    if( ts_received.from_buf( buf ) ) {
      ts_current.set_now();
      ts_current.print_diff( ts_received );
    }
  }

  return 0;
}
